(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elpher-default-url-type "gemini")
 '(package-selected-packages '(elpher magit org auctex)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

; All of my (La)TeX settings
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(setq TeX-engine 'xetex) ; Sets engine to Xe(La)TeX for better font processing and internationalisation
(setq TeX-view-program-selection '((output-pdf "Zathura"))) ; Sets TeX PDF viewer to Zathura
; Block that sets C-c C-] to close any open environment blocks (eg add \end{ENVIRONMENT} to close nearest \begin{ENVIRONMENT}
(add-hook 'TeX-mode-hook ; Must be 'TeX-mode-hook and not 'tex-mode-hook due to use of AUCTeX
	  (lambda ()
	    (local-set-key [3 29] 'latex-close-block)))

; Hook to activate visual-line-mode within Org Mode to wrap lines
(add-hook 'org-mode-hook
	  'visual-line-mode)

; Sets environment variables to use with Emacs
(setenv "DICTIONARY" "en_GB") ; for Ispell dictionary to be set to British English regardless of system locale
(setenv "SHELL" "/usr/bin/fish") ; For term mode to launch fish instead of sh

(load-theme 'manoj-dark) ; Sets theme to Manoj Dark

; Sets fonts to JetBrains Mono with size 11
(add-to-list 'default-frame-alist '(font . "JetBrainsMono Nerd Font Mono-11"))
(set-face-attribute 'default t :font "JetBrainsMono Nerd Font Mono-11")

; Hide the bars around the Emacs window, leaving only the bottom information bar visible
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)

; Loads custom packages installed from sources other than MELPA
(add-to-list 'load-path "~/.emacs.d/site-lisp/")
(load "gemini-mode.el") ; Gemini protocol highlighting, originally from https://github.com/emacsmirror/gemini-mode
