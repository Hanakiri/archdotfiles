# Emacs Config

This is my Emacs config repository, I use an init.el in this folder rather than a .emacs in my home directory to try and keep it a bit less crowded.
I am thinking of potentially moving this folder to ~/.config/emacs in the future to further this concern, but for now it is here.

My config files for Emacs are a bit barren, due to two reasons: I have just started using Emacs (not the first time, but a return) and I have decided to learn it as vanilla as possible (no eVIl mode or any big overhaul like Doom Emacs).

# Notes

1) I have not used Emacs in some time, so these settings might potentially be outdated to whichever current version of Emacs is released.

2) Not listed here, but in my Hyprland config file, I rebind my caps lock key to be an additional control key, and I would recommend anyone trying out Emacs to do the same to avoid pain.
I also recommend not using any overhaul nor eVIl mode like stated above due to conflicting philosophy between VI bindings and Emacs bindings.
I think if you want VI bindings and commands, use VI, otherwise stick with Emacs bindings and just learn them.

3) in my init.el, I load lisp packages from a directory (~/.emacs.d/site-lisp/) and that directory is not in this repository.
For each package loaded in the init file, a URL to the repository of that package will be listed in case one wants to install that package themselves

![logo](../Bambietta.webp "logo"){height=250px}
