# 2024 Arch Dotfiles

This is the new version of my dotfiles as of my 2024 fresh install of Arch Linux.
I planned to copy my old dotfiles over to this one, but decided to start fresh as I have completely switched to Wayland and Hyprland as my main and daily driver.

These dotfiles are being managed by chezmoi.
Traditionally I've used yadm, but I decided I want to try something new to see how it differed from yadm or just using raw git.

![logo](Bambietta.webp "logo"){height=250px}
