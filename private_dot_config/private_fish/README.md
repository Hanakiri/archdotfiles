# Fish Config

Most of this configuration is self-explanatory, however in the main config.fish file I have a check to source my /etc/profile because fish by default cannot handle Arch's /usr/bin/perl_X/ folders and does not source them to the $PATH.

![logo](../../Bambietta.webp "logo"){height=250px}
