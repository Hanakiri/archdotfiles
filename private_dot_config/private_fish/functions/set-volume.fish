function set-volume --wraps='wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@' --description 'alias set-volume wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@'
  wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ $argv
        
end
