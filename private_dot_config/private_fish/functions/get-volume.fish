function get-volume --wraps='wpctl get-volume @DEFAULT_AUDIO_SINK@' --description 'alias get-volume wpctl get-volume @DEFAULT_AUDIO_SINK@'
  wpctl get-volume @DEFAULT_AUDIO_SINK@ $argv
        
end
