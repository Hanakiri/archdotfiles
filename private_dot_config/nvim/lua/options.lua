vim.o.number = true -- Shows line numbers
vim.o.relativenumber = true -- Sets them relative to the cursor
vim.o.hlsearch = true -- Highlights all terms matching a forward or backward search
vim.o.expandtab = true -- Writes out tabs as spaces
vim.o.tabstop = 2 -- Sets tabs to 2 spaces
vim.o.softtabstop = 2 -- Similar as above
vim.o.shiftwidth = 2 -- Any real tabs will be viewed as if they were two spaces wide
vim.o.spelllang = "en_gb" -- Sets spell check language to British English
vim.o.spell = true -- Turns on spell check (can be turned off by running :set spell! when in NeoVim)
vim.cmd("filetype plugin indent on") -- Turns on filetype detection and indents based off the type of file being edited
--vim.o.syntax = true
vim.o.termguicolors = true
vim.cmd("colorscheme koehler")
