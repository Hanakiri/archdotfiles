local Plug = vim.fn['plug#']

-- BEGIN VimPlug call to add plugins --
vim.call('plug#begin')
--Plug('lervag/vimtex')
--Plug('vimwiki/vimwiki')
--Plug('junegunn/goyo.vim')
--Plug('junegunn/limelight.vim')
--Plug('godlygreek/tabular')
--Plug('elzr/vim-json')
--Plug('plasticboy/vim-markdown')
Plug('kaarmu/typst.vim')
--Plug('folke/tokyonight.nvim')
--Plug('rebelot/kanagawa.nvim')
vim.call('plug#end')
-- END --

-- BEGIN setting plugin settings --
--require("plugins/vimtex")
--require("plugins/vimwiki")
--require("plugins/vim-markdown")
--require("plugins/limelight")
require("plugins/typst")
-- END --
