vim.g.vimtex_view_method = 'zathura' -- Sets zathura as the PDF viewer to open when compiling with vimtex
vim.g.vimtex_view_automatic = 0 -- Disables automatic Zathura opening during compilation run
--vim.g.vimtex_compiler_method = 'latexmk' -- Sets latexmk as the main vimtex engine
vim.g.vimtex_compiler_method = 'arara' -- Sets Arara as the compiler
vim.g.vimtex_compiler_latexmk = { out_dir = './build' } -- Creates a directory named 'build' in the folder containing the project's main TeX file and puts all compiled output files (including the output PDF) in this directory
vim.g.vimtex_compiler_latexmk_engines = { _ = '-pdfxe' } -- Uses XeLaTeX as the TeX engine for latexmk
