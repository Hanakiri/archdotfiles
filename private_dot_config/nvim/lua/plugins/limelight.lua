vim.g.limelight_conceal_ctermfg = 'gray'
vim.g.limelight_comceal_ctermfg = 240

vim.g.limelight_conceal_guifg = 'DarkGray'
vim.g.limelight_conceal_guifg = '#777777'

vim.g.limelight_default_coefficient = 0.7

vim.g.limelight_paragraph_span = 1

vim.g.limelight_priority = -1

vim.api.nvim_create_autocmd("User", {
  pattern = "GoyoEnter",
  command = "Limelight",
  desc = "Turns on Limelight when entering Goyo"
})

vim.api.nvim_create_autocmd("User", {
  pattern = "GoyoLeave",
  command = "Limelight!",
  desc = "Turns off Limelight when leaving Goyo"
})
