-- This file is just a hook to load all of the LUA config modules that are in the NeoVim config directory --

require("plugins") -- In <configdir>/lua/plugins/init.lua which contains the VimPlug code to load plugins and <configdir>/lua/plugins/<plug>.lua where each plugin's individual settings are declared
require("options") -- In <configdir>/lua/options.lua and contains all the variables and options to be set when loading NeoVim
